import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import models.Student;

import java.io.IOException;
import java.util.ArrayList;
public class Main {

    public static String customerToJSON(Student student) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(student);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return s;
    }

    public static Student JSONToStudent(String s) {

        ObjectMapper mapper = new ObjectMapper();
        Student student = null;

        try {
            student = mapper.readValue(s, Student.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return student;
    }


    public static void main(String[] args) {
        Student student = new Student();

        ArrayList<String> courses = new ArrayList<>();

        courses.add("CIT241");
        courses.add("CIT240");
        courses.add("WDD100");

        student.setName("Corey");
        student.setAge(28);
        student.setPhone("951-970-7620");
        student.setCourses(courses);


        System.out.println(" Student #1 ");
        String json = Main.customerToJSON(student);
        System.out.println(json);


        Student studentJson = Main.JSONToStudent(json);
        System.out.println(studentJson);

        try{
            System.out.println(studentJson3.getName());
        }catch (NullPointerException ex){
            System.out.println(ex.getMessage());
        }



    }
}